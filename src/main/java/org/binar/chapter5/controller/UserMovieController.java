package org.binar.chapter5.controller;

import org.binar.chapter5.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/users_movie")
public class UserMovieController {
    @Autowired
    UserServiceImpl userService;

    @PostMapping("/new_user")
    public ResponseEntity insertUser(@RequestParam("email") String email,
                                     @RequestParam("password") String password,
                                     @RequestParam("username") String username){
        Map<String, Object> resp = new HashMap<>();

        try{
            resp.put("message", "insert success!");
            resp.put("email", email);
            resp.put("user berhasil ditambahkan", username);
            userService.addNewUser(email, password, username);
            return new ResponseEntity(resp, HttpStatus.CREATED);
    }
        catch (Exception e){
            resp.put("message", "insert gagal!, dikarenakan " + e.getMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PutMapping("/update")
    public ResponseEntity updateUser(@RequestParam("email") String email,
                                     @RequestParam("password") String password,
                                     @RequestParam("username") String username){

        Map<String, Object> resp = new HashMap<>();

        try{userService.updateUser(email, password, username);
            resp.put("message", "update success!");
            resp.put( "user berhasil diupdate", username );
            return new ResponseEntity(resp, HttpStatus.CREATED);
        }
        catch (Exception e){
            resp.put("message", "update gagal!, dikarenakan " + e.getMessage());
            return new ResponseEntity(resp, HttpStatus.INTERNAL_SERVER_ERROR);
        }



    }

    @DeleteMapping("/delete")
    public ResponseEntity deleteUser(@RequestParam("username") String username){
       Map<String, Object> resp = new HashMap<>();
        try{
            resp.put("message", "delete success!");
            resp.put("user berhasil dihapus", username);
            userService.deleteUser(username);
            return new ResponseEntity<>(resp, HttpStatus.ACCEPTED);
        }catch (Exception e){
            resp.put("message", "delete gagal!, dikarenakan " + e.getMessage());
            return new ResponseEntity<>(resp, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }




}
